import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { makeServer } from "./server"
import Vuelidate from 'vuelidate'

import VueSession from 'vue-session'
Vue.use(VueSession)


// resources from doordash
import 'bootstrap/dist/css/bootstrap.min.css'
require('@/assets/styles.css')

import Button from './components/Button/Button.vue'

// re usable compoentns ends


if (process.env.NODE_ENV === "development") {
  makeServer()
}
Vue.use(Vuelidate)
Vue.config.productionTip = false
Vue.component('Button', Button);






new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
