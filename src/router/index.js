import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import StartChallenger from '@/views/StartChallenger.vue'
import UnderAge from '@/views/UnderAge.vue'
import Challenger from '@/views/Challenger.vue'
import ChallengeSuccessful from '@/views/ChallengeSuccessful.vue'
import ChallengeReceiver from '@/views/ChallengeReceiver.vue'
import ChallengeAccepted from '@/views/ChallengeAccepted.vue'
import ChallengeDeclined from '@/views/ChallengeDeclined.vue'
import DeclinedLayout from '@/layouts/DeclinedLayout.vue'
import Appall from '@/layouts/Appall.vue'




Vue.use(VueRouter)

const routes = [
  {

    path: '/challenge-declined',
    name: 'challenge-declined',
    component: DeclinedLayout,
    children: [
      {
        path: '',
        component: ChallengeDeclined
      }
    ],
  },

  {

    path: '/challenge-accepted',
    name: 'challenge-accepted',
    component: Appall,
    children: [
      {
        path: '',
        component: ChallengeAccepted
      }
    ],

  },

  {

    path: '/accept-challenge',
    name: 'accept-challenge',
    component: Appall,
    children: [
      {
        path: '',
        component: ChallengeReceiver
      }
    ],

  },




  // if the challenge is successful

  {
    // if client click challege

    path: '/challenge-successful',
    name: 'challenge-successful',
    component: Appall,
    children: [
      {
        path: '',
        component: ChallengeSuccessful
      }
    ],

  },

  {
    // if client click challege

    path: '/challenger',
    name: 'challenger',
    component: Appall,
    children: [
      {
        path: '',
        component: Challenger
      }
    ],

  },

  {
    // if client click challege

    path: '/start-challenge',
    name: 'start-challenge',
    component: Appall,
    children: [
      {
        path: '',
        component: StartChallenger
      }
    ],

  },

  {
    // if client click challege

    path: '/age-check',
    name: 'age-check',
    component: Appall,
    children: [
      {
        path: '',
        component: UnderAge
      }
    ],

  },

  {
    // if client click challege

    path: '/',
    name: 'Home',
    component: Appall,
    children: [
      {
        path: '',
        component: Home
      }
    ],

  },
]

const router = new VueRouter({
  routes
})

export default router


