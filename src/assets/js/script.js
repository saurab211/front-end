// Bootstrap 5 form validation
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
'use strict'

	// Fetch all the forms we want to apply custom Bootstrap validation styles to
	const forms = document.querySelectorAll('.needs-validation')

	// Loop over them and prevent submission
	Array.prototype.slice.call(forms).forEach(function (form) {
		form.addEventListener('submit', function (event) {
			if (!form.checkValidity()) {
				const firstInvalidInput = form.querySelector('input:invalid')
				firstInvalidInput.scrollIntoView()

				event.preventDefault()
				event.stopPropagation()
			}

			form.classList.add('was-validated')
		}, false)
    })
    
	const flkty = new Flickity('.main-gallery', {
	  prevNextButtons: false,
	  pageDots: false,
	  initialIndex : 2,
	  adaptiveHeight: true
	})
	flkty.on('staticClick', function(event, pointer, cellElement, cellIndex) {
		flkty.select(cellIndex)
	})
	flkty.on('change', function(index) {
		const cell = flkty.getCellElements()[index]
		cell.querySelector('input').checked = true
	})
})()

// Add 'selected-nsw' or 'selected-qld' to the header when selecting team to style form based on selection  
document.querySelector("#blues").addEventListener("change", function() {
    document.body.classList.remove("selected-qld");
    document.body.classList.add("selected-nsw");
});

document.querySelector("#maroons").addEventListener("change", function() {
    document.body.classList.remove("selected-nsw");
    document.body.classList.add("selected-qld");
});